$(document).ready(function() {

    $(".footer__button").click(function(e){
        e.preventDefault();
        var self = $(this);
        if(self.hasClass("footer__button_light")){
            $(".footer__item").removeClass("footer__item_active");
            self.closest(".footer__item").addClass("footer__item_active").closest(".page").addClass("page_light");
        }
        if(self.hasClass("footer__button_dark")){
            $(".footer__item").removeClass("footer__item_active");
            self.closest(".footer__item").addClass("footer__item_active").closest(".page").removeClass("page_light");
        }
    });

	$(".js-fancybox").fancybox({
		padding: 0,
        closeBtn: false,
        fitToView: false
	});

	$('#fancyboxSwitcher').change(function(){
        if ($(this).prop('checked')) {
            $('.js-part').removeClass('js-part-active');
        } else {
            $('.js-part').addClass('js-part-active');
        }
    });

    $(".select").customSelect({});

    $(".list").hide();


    // count
    var counts = $(".count");

    for (var i = 0; i < counts.length; i++){
        if(parseInt($(counts[i]).find(".count__count").text()) != 1){
            $(counts[i]).find(".count__button_reduce").removeClass("count__button_disable");
        }
    }

    $(".count__button").click(function(e){
        e.preventDefault();
        var wrapper = $(this).closest(".product");
        var countBlock = wrapper.find(".count__count");
        var count = parseInt(countBlock.text());
        if ($(this).hasClass("count__button_add")){
            count += 1;
            wrapper.find(".count__button_reduce").removeClass("count__button_disable");
        } else {
            count -= 1;
            if (count <= 1) {
                wrapper.find(".count__button_reduce").addClass("count__button_disable");
                count = 1;
            }
        }
        countBlock.text(count);
    })

    //accordion
    $(".accordion__title").click(function(){
        var parentBlock = $(this).closest(".accordion");
        if (parentBlock.hasClass("accordion_active")){
            parentBlock.find(".accordion__body").slideUp(500);
            parentBlock.removeClass("accordion_active");
        } else {
            parentBlock.find(".accordion__body").slideDown(500);
            parentBlock.addClass("accordion_active");
        }
    });

    //provider
    $(".provider__title").click(function(e){
        e.preventDefault();
        var parentBlock = $(this).closest(".provider");
        if (parentBlock.hasClass("provider_active")){
            parentBlock.removeClass("provider_active");
            parentBlock.find(".provider__info").slideUp(500);
        } else {
            parentBlock.addClass("provider_active");
            parentBlock.find(".provider__info").slideDown(500);
        }
    });

    //list
    $(".order-item__count").click(function(e){
        e.preventDefault();
        var parentBlock = $(this).closest(".order");
        if ($(this).hasClass("order-item__count_active")){
            $(this).removeClass("order-item__count_active");
            parentBlock.find(".list").slideUp(500);
        } else {
            $(this).addClass("order-item__count_active");
            parentBlock.find(".list").slideDown(500);
        }
    });

    $(".list__close").click(function(e){
        e.preventDefault();
        var parentBlock = $(this).closest(".order");
        parentBlock.find(".order-item__count").removeClass("order-item__count_active");
        parentBlock.find(".list").slideUp(500);
	});

    $(".order-item__link_mail").click(function(e){
        e.preventDefault();
        var parentBlock = $(this).closest(".order");
        if ($(this).hasClass("order-item__link_active")){
            $(this).removeClass("order-item__link_active");
            parentBlock.find(".list").slideUp(500);
        } else {
            $(this).addClass("order-item__link_active");
            parentBlock.find(".list").slideDown(500);
        }
    });

    $(".list__cancer").click(function(e){
        e.preventDefault();
        var parentBlock = $(this).closest(".order");
        parentBlock.find(".order-item__link_mail").removeClass("order-item__link_active");
        parentBlock.find(".list").slideUp(500);
    });

    $(".list__ok").click(function(e){
        e.preventDefault();
        var parentBlock = $(this).closest(".order");
        var text = parentBlock.find('.list__textarea').val();
        parentBlock.find(".order-item__cell_text").append("<div class='order-item__text order-item__text_answer'>" + text +"</div>");
        parentBlock.find(".order-item__link_mail").removeClass("order-item__link_active");
        parentBlock.find(".list").slideUp(500);
    });

    $.datepicker.setDefaults($.datepicker.regional['ru']);

    $('#datepicker').datepicker({
        showOn: "button",
        buttonText: "Дата"
    });

    //switcher
    $(".switcher__label").click(function(e){
        e.preventDefault();
        var self = $(this);
        var parentBlock = self.closest(".switcher");
        var switcher = parentBlock.find(".switcher__input");
        if (self.hasClass("switcher__label_active") && !switcher.prop('checked')){
            switcher.prop('checked', true);
            console.log("true");
        }
        if (self.hasClass("switcher__label_unactive") && switcher.prop('checked')){
            switcher.prop('checked', false);
            console.log("false");
        }
    });

    //pre-order
    //todo: незаморачивался. Стоит грамотно переделать, но полноценно не понимал что это должно делать
    if ($(".card__pre-order").length == 1){
        var elem = $(".card__pre-order");
        var parentBlock = elem.closest(".card");
        var preProduct = parentBlock.find(".product_pre-order");
        var delta = preProduct.eq(1).position().top - preProduct.eq(0).position().top - 32; // 32px промежуток для написания количества
        preProduct.eq(0).find(".product__pre").show().css({
            height: delta/2,
            top: "25px"
        });
        var pre = preProduct.eq(1);
        var count = pre.data("count");
        pre.find(".product__pre").show().css({
            height: delta/2,
            bottom: "25px"
        }).append("<div class='product__pre-count'>"+count+"</div>");
        parentBlock.find(".product__pre-count").css("bottom", delta/2);
    }

    //active menu
    //todo: убрать на продакшене. Добавил, чтобы можно было смотреть активность на презентации
    var path = window.location.pathname;
    path = path.split("/");
    path = path[path.length-1];
    path = "./"+ path;
    var menuLinks = $(".menu__link");
    for (var i = 0; i < menuLinks.length; i++){
        var itemMenu = menuLinks.eq(i);
        if(itemMenu.attr("href") == path ){
            itemMenu.closest(".menu__item").addClass("menu__item_active");
        }
    }
});